// require([
//   "esri/Map",
//   "esri/views/MapView",
//   "esri/layers/CSVLayer"
// ], function (Map, MapView, CSVLayer) {

//   let map = new Map({
//     basemap: "topo-vector"
//   });

//   let USAView = new MapView({
//     container: "USA",
//     map: map,
//     center: [-95.7129, 37.0902], // longitude, latitude
//     zoom: 3
//   });


//   let WorldView = new MapView({
//     container: "World",
//     map: map,
//     zoom: 2
//   });

//   // If CSV files are not on the same domain as your website, a CORS enabled server
//   // or a proxy is required.
//   let USACsvFileUrl =
//     "https://developers.arcgis.com/javascript/latest/sample-code/layers-csv/live/earthquakes.csv";

//   let WorldCsvFileUrl =
//     "https://developers.arcgis.com/javascript/latest/sample-code/layers-csv/live/earthquakes.csv";
  


//   const popupTemplate = {
//     content: "{place}"
//   };

//   let iconRenderer = {
//     type: "simple",
//     symbol: {
//       type: "simple-marker",

//       color: [
//         0,
//         197,
//         255,
//         64
//       ],
//       size: 30,
//       xoffset: 0,
//       yoffset: 15,
//       path: "M16,3.5c-4.142,0-7.5,3.358-7.5,7.5c0,4.143,7.5,18.121,7.5,18.121S23.5,15.143,23.5,11C23.5,6.858,20.143,3.5,16,3.5z M16,14.584c-1.979,0-3.584-1.604-3.584-3.584S14.021,7.416,16,7.416S19.584,9.021,19.584,11S17.979,14.584,16,14.584z"

//     }
//   }

//   let locationLabels = {
//     symbol: {
//       type: "text",
//       color: "#000000",
//       haloColor: "#FFFFFF",
//       haloSize: "1px",
//       font: {
//         size: "15px",
//         family: "Arial",
//         style: "normal",
//         weight: "bold"
//       }
//     },
//     labelPlacement: "center-center",
//     labelExpressionInfo: {
//       expression: "$feature.place"
//     }
//   };

//   let USACsvLayer = new CSVLayer({
//     url: USACsvFileUrl,
//     labelingInfo: [locationLabels],
//     renderer: iconRenderer,
//     popupTemplate: popupTemplate
//   });

//   let WorldCsvLayer = new CSVLayer({
//     url: WorldCsvFileUrl,
//     labelingInfo: [locationLabels],
//     renderer: iconRenderer,
//     popupTemplate: popupTemplate
//   });

//   map.add(USACsvLayer);
//   map.add(WorldCsvLayer);


// });


























require([
  "esri/Map",
  "esri/views/MapView",
  "esri/layers/CSVLayer"
], function (Map, MapView, CSVLayer) {

  // If CSV files are not on the same domain as your website, a CORS enabled server
  // or a proxy is required.
  let USACsvFileUrl =
    "USA.csv";

  let WorldCsvFileUrl =
    "World.csv";
  


  const popupTemplate = {
    content: "{place}"
  };

  let iconRenderer = {
    type: "simple",
    symbol: {
      type: "simple-marker",

      color: [
        0,
        197,
        255,
        64
      ],
      size: 30,
      xoffset: 0,
      yoffset: 15,
      path: "M16,3.5c-4.142,0-7.5,3.358-7.5,7.5c0,4.143,7.5,18.121,7.5,18.121S23.5,15.143,23.5,11C23.5,6.858,20.143,3.5,16,3.5z M16,14.584c-1.979,0-3.584-1.604-3.584-3.584S14.021,7.416,16,7.416S19.584,9.021,19.584,11S17.979,14.584,16,14.584z"

    }
  }

  let locationLabels = {
    symbol: {
      type: "text",
      color: "#000000",
      haloColor: "#FFFFFF",
      haloSize: "1px",
      font: {
        size: "15px",
        family: "Arial",
        style: "normal",
        weight: "bold"
      }
    },
    labelPlacement: "center-center",
    labelExpressionInfo: {
      expression: "$feature.place"
    }
  };




  document.getElementById("WorldButton").addEventListener("click", SetWorld);
  document.getElementById("USAbutton").addEventListener("click", SetUSA);

  function SetUSA(){
        console.log("############")

        let USAMap = new Map({
          basemap: "topo-vector"
        });
      
        let USAView = new MapView({
          container: "USA",
          map: USAMap,
          center: [-95.7129, 37.0902], // longitude, latitude
          zoom: 3
        });

        let USACsvLayer = new CSVLayer({
          url: USACsvFileUrl,
          labelingInfo: [locationLabels],
          renderer: iconRenderer,
          popupTemplate: popupTemplate
        });

        USAMap.add(USACsvLayer);

        document.getElementById("USA1").style.display = "block";
        document.getElementById("World1").style.display = "none";
      
  }

  function SetWorld(){
        console.log("###########@@@@@@@@@")

        let WorldMap = new Map({
          basemap: "topo-vector"
        });

        let WorldView = new MapView({
          container: "World",
          map: WorldMap,
          zoom: 2
        });

        let WorldCsvLayer = new CSVLayer({
          url: WorldCsvFileUrl,
          labelingInfo: [locationLabels],
          renderer: iconRenderer,
          popupTemplate: popupTemplate
        });

        WorldMap.add(WorldCsvLayer);

        document.getElementById("USA1").style.display = "none";
        document.getElementById("World1").style.display = "block";
      
  }

});

